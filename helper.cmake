set(UTILITY_FUNCTIONS_CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR})
set(UTILITY_FUNCTIONS_CMAKE_FILE ${CMAKE_CURRENT_LIST_FILE})

function(add_path_to_msvc_project_file)
    set (OPTIONS)
    set (ONE_VALUE_ARGS TARGET)
    set (MULTI_VALUE_ARGS PATHS_DEBUG PATHS_RELEASE)

    cmake_parse_arguments(_arg "${OPTIONS}" "${ONE_VALUE_ARGS}" "${MULTI_VALUE_ARGS}" ${ARGN})

    if(_arg_PATHS_DEBUG)
        set(VCX_LOCAL_DEBUGGER_ENVIRONMENT_DEBUG "PATH=${_arg_PATHS_DEBUG};%PATH%")
    endif()

    if(_arg_PATHS_RELEASE)
        set(VCX_LOCAL_DEBUGGER_ENVIRONMENT_RELEASE "PATH=${_arg_PATHS_RELEASE};%PATH%")
    endif()

    # By default, configure_file sets the attributes of the input file also on the output file.
    # In our case, the input file is readonly (as it is in a workspace of perforce)
    # Thus we first configure it into a temp file and then copy it without the attributes
    # to the final place. CMake's configure_file doesn't allow setting attributes directly, thus the indirection.
    configure_file("${UTILITY_FUNCTIONS_CMAKE_DIR}/project.vcxproj.user.in" 
                   "${CMAKE_CURRENT_BINARY_DIR}/temp/${_arg_TARGET}.vcxproj.user" @ONLY)

    file(COPY        "${CMAKE_CURRENT_BINARY_DIR}/temp/${_arg_TARGET}.vcxproj.user"
         DESTINATION "${CMAKE_CURRENT_BINARY_DIR}/"
         NO_SOURCE_PERMISSIONS)

    # Cleanup
    if (NOT (${CMAKE_CURRENT_BINARY_DIR} STREQUAL "")) # just making sure we don't delete a root dir
        file(REMOVE_RECURSE "${CMAKE_CURRENT_BINARY_DIR}/temp/")
    endif()
endfunction()