/***********************************************************
 ***   THIS DOCUMENT CONTAINS PROPRIETARY INFORMATION.   ***
 ***    IT IS THE EXCLUSIVE CONFIDENTIAL PROPERTY OF     ***
 ***       STRYKER CORPORATION AND ITS AFFILIATES.       ***
 ***                                                     ***
 ***             Copyright (C) 2021 Stryker              ***
 ***********************************************************/

#include "color_coded_mask_mouse_area.h"

#include <QQmlFile>
#include <QColor>
#include <QPainter>
#include <QtSvg/QSvgRenderer>
#include <QGuiApplication>
#include <QCoreApplication>
#include <QStyleHints>
#include <iostream>

ColorCodedMaskMouseArea::ColorCodedMaskMouseArea(QQuickItem *parent /*= 0*/)
    : QQuickItem(parent),
    _pressed(false),
    _current_pixel_value(QColor().black())
{
    setAcceptHoverEvents(true);
    setAcceptedMouseButtons(Qt::LeftButton);
}

void ColorCodedMaskMouseArea::renderNewMaskSource()
{
    auto file = QQmlFile::urlToLocalFileOrQrc(_mask_source);

    if (!file.toLower().endsWith("svg")) {
        qWarning() << "Mask source must be of type svg";
        return;
    }

    QSvgRenderer renderer(file);
    //renderer.si

    QImage image(width(), height(), QImage::Format_ARGB32);
    image.fill(QColor().black());

    QPainter painter(&image);
    renderer.render(&painter);

    _mask = image;
}

void ColorCodedMaskMouseArea::setMaskSource(const QUrl& source)
{
    if (_mask_source != source) {
        _mask_source = source;

        renderNewMaskSource();

        emit maskSourceChanged();
    }
}

void ColorCodedMaskMouseArea::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    if (_mask_source.isEmpty()) {
        return;
    }

    renderNewMaskSource();
    
    _mask.save("c:/temp/mask2.png");

    std::cout << "Size changed. New mask size: " << _mask.width() << " x " << _mask.height() << std::endl;
}

void ColorCodedMaskMouseArea::setCurrentPixelValue(QColor color)
{
    if (color != _current_pixel_value) {
        _current_pixel_value = color;
        emit colorUnderMouseChanged();
    }
}

void ColorCodedMaskMouseArea::setPressed(bool pressed)
{
    if (pressed != _pressed) {
        _pressed = pressed;
        emit pressedChanged();
    }
}

void ColorCodedMaskMouseArea::hoverMoveEvent(QHoverEvent* event)
{
    if (_mask.isNull()) {
        return;
    }

    if (!_mask.valid(event->pos())) {
        return;
    }

    //std::cout << "Hover: " << event->pos().x() << "/" << this->width()
    //    << ", " << event->pos().y() << "/" << this->height() << " - ";
    auto rgba = _mask.pixel(event->pos());
    QColor color;
    color.setRgba(rgba);
    std::cout << "Color: " << color.name().toStdString() << std::endl;

    if (_mask.valid(event->pos())) {
        setCurrentPixelValue(color);
    } else {
        setCurrentPixelValue(QColor().black());
    }
}

void ColorCodedMaskMouseArea::mousePressEvent(QMouseEvent *event)
{
    setPressed(true);
    _pressed_position = event->pos();
    emit pressed();
}

void ColorCodedMaskMouseArea::mouseReleaseEvent(QMouseEvent *event)
{
    setPressed(false);
    emit released();

    const int threshold = qApp->styleHints()->startDragDistance();
    const bool is_click = (threshold >= qAbs(event->x() - _pressed_position.x()) &&
        threshold >= qAbs(event->y() - _pressed_position.y()));

    if (is_click) {
        emit clicked(event);
    }
}
