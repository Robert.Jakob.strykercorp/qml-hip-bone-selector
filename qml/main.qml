import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15
import QtQuick.Window 2.2
import ColorCodedMaskMouseArea 1.0

Window {
    visible: true

    width: 600
    height: 400

	Rectangle {
        id: rect
        anchors.fill: parent

        color: "black"
    }

	Text {
		id: text
		anchors.leftMargin: 20
		anchors.top: area.bottom
		text: ""
		color: "white"
		font.pointSize: 15
	}

	ColorCodedMaskMouseArea {
		id: area

		width: 300
		height: 300

		implicitWidth: 94
		implicitHeight: 94

		maskSource: "Pelvis Mini Selector Color Coded hip-selector.svg"

		onClicked: {
			console.log("clicked")		
		}

		onColorUnderMouseChanged: {

			if (Qt.colorEqual(colorUnderMouse, "#00FFFF")) {
				console.log("Left Femur")
				hipBoneSelector.state = 0
				hipBoneSelector.bone = 2
				text.text = "That's the left femur"
			} else if (Qt.colorEqual(colorUnderMouse, "#FFFF00")) {
				console.log("Right Femur")
				hipBoneSelector.state = 0
				hipBoneSelector.bone = 3
				text.text = "That's the right femur"
			} else if (Qt.colorEqual(colorUnderMouse, "#FF00FF")) {
				console.log("Left Hemi Pelvis")
				hipBoneSelector.state = 0
				hipBoneSelector.bone = 4
				text.text = "That's the left hemi pelvis"
			} else if (Qt.colorEqual(colorUnderMouse, "#FF0000")) {
				console.log("Right Hemi Pelvis")
				hipBoneSelector.state = 0
				hipBoneSelector.bone = 5
				text.text = "That's the right hemi pelvis"
			} else {
				console.log("No known color")
				hipBoneSelector.state = 0
				hipBoneSelector.bone = 0
				text.text = ""
			}
		}

		HipBoneSelector {
			id: hipBoneSelector
			anchors.fill: parent

			expectedWidth: 300
			expectedHeight: 300
		}
	}

	HipBoneSelector {
		id: hipBoneSelectorQuadrants

		x: 300
		y: 0
		width: 300
		height: 300
		
		expectedWidth: 300
		expectedHeight: 300

		GridLayout {
			rows: 2
			columns: 2

			anchors.fill: parent

			MouseArea {
				Layout.fillWidth: true
				Layout.fillHeight: true
				hoverEnabled: true

				onEntered: {
					hipBoneSelectorQuadrants.bone = 5
					console.log("top-left")
				}
				onExited: {
					hipBoneSelectorQuadrants.bone = 0
				}
			}

			MouseArea {
				Layout.fillWidth: true
				Layout.fillHeight: true
				hoverEnabled: true

				onEntered: {
					hipBoneSelectorQuadrants.bone = 4
					console.log("top-right")
				}
				onExited: {
					hipBoneSelectorQuadrants.bone = 0
				}
			}
			
			MouseArea {
				Layout.fillWidth: true
				Layout.fillHeight: true
				hoverEnabled: true

				onEntered: {
					hipBoneSelectorQuadrants.bone = 3
					console.log("bottom-left")
				}
				onExited: {
					hipBoneSelectorQuadrants.bone = 0
				}
			}

			MouseArea {
				Layout.fillWidth: true
				Layout.fillHeight: true
				hoverEnabled: true

				onEntered: {
					hipBoneSelectorQuadrants.bone = 2
					console.log("bottom-right")
				}
				onExited: {
					hipBoneSelectorQuadrants.bone = 0
				}
			}
		}
	}
}