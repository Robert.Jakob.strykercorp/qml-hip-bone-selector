#pragma once
/***********************************************************
 ***   THIS DOCUMENT CONTAINS PROPRIETARY INFORMATION.   ***
 ***    IT IS THE EXCLUSIVE CONFIDENTIAL PROPERTY OF     ***
 ***       STRYKER CORPORATION AND ITS AFFILIATES.       ***
 ***                                                     ***
 ***             Copyright (C) 2021 Stryker              ***
 ***********************************************************/

#include <QImage>
#include <QQuickItem>

//! A mouse area that can be given a mask (arbitrary image) and queried which color of the
//! mask is currently under the cursor.
class ColorCodedMaskMouseArea : public QQuickItem
{
public:
    Q_OBJECT
    Q_PROPERTY(bool pressed READ isPressed NOTIFY pressedChanged)
    Q_PROPERTY(QUrl maskSource READ maskSource WRITE setMaskSource NOTIFY maskSourceChanged)
    Q_PROPERTY(QColor colorUnderMouse READ colorUnderMouse NOTIFY colorUnderMouseChanged)

public:
    ColorCodedMaskMouseArea(QQuickItem *parent = 0);

    bool isPressed() const { return _pressed; }

    QUrl maskSource() const { return _mask_source; }
    void setMaskSource(const QUrl &source);

    QColor colorUnderMouse() const { return _current_pixel_value; }

signals:
    void pressed();
    void released();
    void clicked(QMouseEvent* mouse);
    void maskSourceChanged();
    void pressedChanged();
    void enabledChanged();
    void colorUnderMouseChanged();

protected:
    void hoverMoveEvent(QHoverEvent* event) override;
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);

private:
    bool _pressed;
    bool _contains_mouse;
    QUrl _mask_source;
    QColor _current_pixel_value;
    QImage _mask;
    QPoint _pressed_position;

    void renderNewMaskSource();

    void setCurrentPixelValue(QColor color);
    void setPressed(bool pressed);
};